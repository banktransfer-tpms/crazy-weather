require 'sinatra'
require 'faraday'
require 'json'

API_KEY = '242c73adcebf00e1aaecd29bb2e46d31'
BASE_URL = 'https://api.openweathermap.org/data/2.5'

get '/v1/weather/hello-world' do
  'TPM Training in a nice weather!'
end

get '/v1/weather/cities/:city' do
    city = params['city']
   data = Faraday.get("#{BASE_URL}/forecast?q=#{city},us&units=metric&APPID=#{API_KEY}")
   weather = JSON.parse(data.body)
   puts weather
   temp = weather['list'][0]['main']['temp']
   # "Temperature in #{city} is #{temp} celcius "
   "The temperature in \"Capitalized\" #{city} is #{temp}" + "\xC2\xB0" + " celcius"
end
  